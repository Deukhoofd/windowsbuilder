FROM archlinux/archlinux:latest

ENV WINEARCH win64
ENV WINEDLLOVERRIDES mscoree,mshtml=d
ENV SDL_AUDIODRIVER=dummy
ENV CHOST x86_64-w64-mingw32
ENV AR x86_64-w64-mingw32-ar
ENV AS x86_64-w64-mingw32-as
ENV RANLIB x86_64-w64-mingw32-ranlib
ENV CC x86_64-w64-mingw32-gcc
ENV CXX x86_64-w64-mingw32-g++
ENV STRIP x86_64-w64-mingw32-strip
ENV R: x86_64-w64-mingw32-windres


RUN echo $'[multilib]\nInclude = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm wget valgrind git cmake ninja shadow sudo
RUN pacman -S --noconfirm gcc

RUN pacman -S --noconfirm wine-staging
RUN pacman -S --noconfirm mingw-w64-gcc


# RUN wineboot -i
